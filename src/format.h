#ifndef FORMAT_H
#define FORMAT_H
#include <iostream>

/**
 * \class Format
 * \brief Definition of the class Format
 */
class Format
{
 public:

  /**
   * \brief Default constructor of Format
   */
  Format();

  /**
  * \brief Constructor with parameters
  * \param[in] id the identification number of a format
  * \param[in] type thex type of a format
  */
  Format(unsigned int, std::string);

  /**
   * \brief Default destructor of Format
   */
  ~Format();

  /**
   * \brief Lets you know the id of a format
   * \return unsigned int representing the id of a format
   */
  unsigned int getId();

  /**
   * \brief Lets you know the type of a format
   * \return string representing the type of a format
   */
  std::string getType();

  /**
   * \brief to set the type of a format
   */
  void setType(std::string);

 private:
  unsigned int id; ///< Attribute defining the id of the format
  std::string type; ///< Attribute defining the type of the format
};

#endif // FORMAT_H
