#include "playlist.h"
#include <fstream>
#include <Xspf.h>
#include <XspfWriter.h>
#include <XspfTrack.h>
#include <XspfIndentFormatter.h>
#include <libpq-fe.h>

Playlist::Playlist():
  id(0),
  title(""),
  duration(0)
{}

Playlist::Playlist(unsigned int _id, std::string _title, std::chrono::minutes _duration):
  id(_id),
  title(_title),
  duration(_duration)
{}

Playlist::~Playlist()
{}

unsigned int Playlist::getId()
{
  return id;
}

std::chrono::minutes Playlist::getDuration()
{
  return std::chrono::duration_cast<std::chrono::minutes>(duration);
}

void Playlist::setDuration(std::chrono::minutes _duration)
{
  duration = _duration;
}

void Playlist::addTrack(Track t)
{
  tracks.push_back(t);
}

void Playlist::writeM3U()
{
  std::string filename = title + ".m3u";
  {
    std::ofstream m3u_stream(filename, std::ios::out);

    if(m3u_stream.is_open())
    {
      for(Track current_track : tracks)
      {
        m3u_stream << current_track.getPath() << std::endl;
      }
    }
  }
}

void Playlist::writeXSPF()
{
  std::string filename = title;
  Xspf::XspfIndentFormatter formatter;
  XML_Char const *const baseUri = _PT("http://radio6mic.net/");
  Xspf::XspfWriter *const writer = Xspf::XspfWriter::makeWriter(formatter, baseUri);
  Xspf::XspfProps *props = new Xspf::XspfProps();
  srand(time(NULL));

  props->lendTitle(_PT(title.c_str()));

  writer->setProps(props);
  for(Track current_track : tracks)
  {
    Xspf::XspfTrack track;
    track.giveTitle(_PT(current_track.getName().c_str()), true);

    track.giveAppendLocation(_PT(current_track.getPath().c_str()), true);
//    track.giveCreator(_PT(current_track.getArtists).c_str()), true);
    writer->addTrack(track);
  }
  writer->writeFile((_PT(title + ".xspf")).c_str());
  
}

void Playlist::DoIt(std::string requete)
{
  char info_connexion[] = "host=postgresql.bts-malraux72.net port=5432 user=c.lemeur dbname=Cours password=P@ssword";
  PGPing ping = PQping(info_connexion);

  if(ping == PQPING_OK)
  {
    PGconn *connexion = PQconnectdb(info_connexion);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      std::chrono::seconds durationReal(0);
      std::chrono::seconds marge = duration * 20 / 100;

      while(marge > durationReal)
      {
        // exec
        PGresult *res = PQexec(connexion, requete.c_str());

        if(PQresultStatus(res) == PGRES_TUPLES_OK)
        {
          durationReal += std::chrono::seconds(((std::atol(PQgetvalue(res, 0, 0)))));

          if(durationReal < duration)
          {
            Track t;
            // valoriser
            t.setDuration(std::chrono::seconds(std::atol(PQgetvalue(res, 0, 0))));
            t.setName(std::string(PQgetvalue(res, 0, 2)));
            t.setPath(std::string(PQgetvalue(res, 0, 3)));
            addTrack(t);
            std::string unArtist = std::string(PQgetvalue(res, 0, 1));
            Artist a;
            a.setName(unArtist);
            t.getArtists().push_back(a);
          }
        }
      }
    }
  }
}
