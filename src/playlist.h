#ifndef PLAYLIST_H
#define PLAYLIST_H
#include "track.h"
#include <iostream>
#include <vector>
#include <chrono>
#include <string>
#include <cstring>

/**
 * \class Playlist
 * \brief Definition of the class Playlist
 */
class Playlist
{
 public:

  /**
   * \brief Default constructor of Playlist
   */
  Playlist();

  /**
   * \brief Constructor with parameters
   * \param id the identification number of a playlist
   * \param title the title of a playlist
   * \param duration the duration of a playlist
   * \param tracks a list of tracks in the playlist
   */
  Playlist(unsigned int, std::string, std::chrono::minutes);

  /**
   * \brief Default destructor of Playlist
   */
  ~Playlist();

  /**
   * \brief Lets you know the id of a playlist
   * \return unsigned int representing the id of a playlist
   */
  unsigned int getId();

  /**
   * \brief Lets you know the duration of a playlist
   * \return std::chrono::minutes representing the duration of a playlist
   */
  std::chrono::minutes getDuration();

  /**
   * \brief to set the duration of a playlist
   */
  void setDuration(std::chrono::minutes);

  /**
   * \brief Lets you add a Track in the list of tracks
   */
  void addTrack(Track);

  /**
   * _brief Write out the playlist to M3U file format
   *
   * The aim of this method is to write the playlist content to the
   * output file named "PLAYLIST" for the moment (waiting to use an
   * attribute with the playlist name)

   */
  void writeM3U();

  /**
   * \brief Write out the playlist to XSPF file format
   *
   * Same as Playlist::writeM3U but the output format file is XSPF,
   * using the \c libxspf library.
   */
  void writeXSPF();

  /**
   * \brief Performs requests in order to create the list of songs according to the duration of the playlist
   */
  void DoIt(std::string);

 private:
  unsigned int id; ///< Attribute defining the id of the playlist
  std::string title; ///< Attribute defining the title of the playlist
  std::chrono::seconds duration; ///< Attribute defining the duration of the playlist
  std::vector<Track> tracks; ///< Attribute defining a list of Track in the playlist
};
#endif // PLAYLIST_H
