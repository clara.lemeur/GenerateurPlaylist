#include "album.h"
#include <chrono>
#include <iostream>

Album::Album():
  id(0),
  name("")
{
  date.std::chrono::system_clock::now();
}


Album::Album(unsigned int _id, std::string _name, std::chrono::system_clock _date):
  id(_id),
  name(_name),
  date(_date)
{}

Album::~Album() {}

std::chrono::system_clock Album::getDate()
{
  return date;
}

unsigned int Album::getId()
{
  return id;
}

std::string Album::getName()
{
  return name;
}

void Album::setName(std::string _name)
{
  name = _name;
}

void Album::setDate(std::chrono::system_clock _date)
{
  date = _date;
}
