#include <libpq-fe.h>
#include <iostream>
#include "cli.h"
#include "playlist.h"
int main(int argc, char *argv[])
{
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  std::string nom = FLAGS_name;
  std::string genre = FLAGS_genre;
  std::string type = FLAGS_type;
  std::string album = FLAGS_album;
  std::string artist = FLAGS_artist;
  std::string title = FLAGS_title ;
  unsigned int duree = FLAGS_duration;
  
  std::string requete = "";requete = "SET SCHEMA 'radio_libre'; select * from \"morceau\" inner join \"artiste_morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceau inner join \"artiste\" on \"artiste_morceau\".id_artiste = \"artiste\".id inner join \"album_morceau\" on \"morceau\".id = \"album_morceau\".id_morceau inner join \"album\" on \"album_morceau\".id_album = \"album\".id inner join \"genre\" on \"morceau\".fk_genre = \"genre\".id where \"genre\".type ~ '" + genre + "' AND \"album\".nom  ~ '" + album + "' AND \"artiste\".nom ~ '" + artist + "' AND \"morceau\".nom ~ '" + title + "' ORDER BY random() LIMIT 1;";
  
  Playlist laPlaylist(1, nom, std::chrono::minutes(duree));

  laPlaylist.DoIt(requete);
  
  laPlaylist.writeM3U();

  laPlaylist.writeXSPF();

  return 0;
}
