#include "polyphony.h"
#include <iostream>

Polyphony::Polyphony():
  Polyphony(0, 0)
{}

Polyphony::Polyphony(unsigned int _id, unsigned _number):
  id(_id),
  number(_number)
{}

Polyphony::~Polyphony()
{}


unsigned int Polyphony::getId()
{
  return id;
}

unsigned int Polyphony::getNumber()
{
  return number;
}

void Polyphony::setNumber(unsigned int _number)
{
  number = _number;
}
