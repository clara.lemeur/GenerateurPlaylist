#include "genre.h"
#include <iostream>

Genre::Genre():
  Genre(0, "")
{}

Genre::Genre(unsigned int _id, std::string _type):
  id(_id),
  type(_type)
{}

Genre::~Genre() {}

unsigned int Genre::getId()
{
  return id;
}

std::string Genre::getType()
{
  return type;
}

void Genre::setType(std::string _type)
{
  type = _type;
}
